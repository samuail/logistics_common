require 'mkscommon/methodresponse'
require 'mkscommon/util'
module Mks
  module LogisticsCommon
    class ApplicationController < ActionController::Base
      protect_from_forgery with: :null_session
    end
  end
end
