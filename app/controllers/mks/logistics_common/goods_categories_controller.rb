module Mks
  module LogisticsCommon
    class GoodsCategoriesController < ApplicationController
      protect_from_forgery with: :null_session
      before_action :set_goods_category, only: [:update]

      # GET /goods_categories
      # GET /goods_categories.json
      def index
        parent_goods_categories = GoodsCategory.roots
        root = {}
        goods_category_tree = []
        if parent_goods_categories.count > 0
          parent_goods_categories.each do |goods_category|
            sub_tree = create_tree(nil, goods_category)
            goods_category_tree.push(sub_tree)
          end
          root['data'] = goods_category_tree
          root['expanded'] = true
          root['code'] = 'Goods Category'
        end
        response = MethodResponse.new(true, nil, root, nil, nil)
        render json: response
      end

      def create_tree parent, goods
        goods_category = GoodsCategory.create_goods_category_detail parent, goods
        if goods.children.count > 0
          goods.children.each do |good|
            create_tree(goods_category, good)
          end
        end
        return goods_category
      end

      # POST /goods_categories
      # POST /goods_categories.json
      def create
        goods_category_type = GoodsCategoryType.find goods_category_params[:goods_category_type_id]
        @goods_category = GoodsCategory.new(goods_category_params)

        if @goods_category.save
          response = MethodResponse.new(true,  "#{goods_category_type.name}" + " added successfully!", @goods_category, nil, nil)
        else
          errors = Util.error_messages @goods_category, "#{goods_category_type.name}" + " Goods"
          response = MethodResponse.new(false, nil, nil, errors, nil)
        end
        render json: response
      end

      # PATCH/PUT /goods_categories/1
      # PATCH/PUT /goods_categories/1.json
      def update
        goods_category_type = GoodsCategoryType.find goods_category_params[:goods_category_type_id]
        @goods_category = GoodsCategory.find(params[:id])

        if @goods_category.update(goods_category_params)
          response = MethodResponse.new(true, "#{goods_category_type.name}" + " updated successfully!", @goods_category, nil, nil)
        else
          errors = Util.error_messages @goods_category, "#{goods_category_type.name}" + " Goods"
          response = MethodResponse.new(false, nil, nil, errors, nil)
        end
        render json: response
      end

      private

        def set_goods_category
          @goods_category = GoodsCategory.find(params[:id])
        end

        def goods_category_params
          params.require(:goods_category).permit(:code, :name, :description, :goods_category_type_id, :ancestry,
                                                 :hs_code, :tariff_code, :unit_of_measure, :duty_rate, :special_permission,
                                                 :excise_tax, :vat, :sur_tax, :withholding_tax, :ss_1, :ss_2)
        end
    end
  end
end
