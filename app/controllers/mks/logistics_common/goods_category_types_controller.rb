module Mks
  module LogisticsCommon
    class GoodsCategoryTypesController < ApplicationController
      def index
        @goods_category_types = GoodsCategoryType.all
        response = MethodResponse.new(true, nil, @goods_category_types, nil, nil)
        render json: response
      end
    end
  end
end
