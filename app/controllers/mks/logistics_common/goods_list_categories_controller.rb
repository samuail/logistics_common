module Mks
  module LogisticsCommon
    class GoodsListCategoriesController < ApplicationController
      protect_from_forgery with: :null_session

      # GET /goods_list_categories
      # GET /goods_list_categories.json
      def index
        @goods_list_categories = GoodsListCategory.all
        response = MethodResponse.new(true, nil, @goods_list_categories, nil, nil)
        render json: response
      end
    end
  end
end
