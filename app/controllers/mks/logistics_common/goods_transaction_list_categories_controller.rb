module Mks
  module LogisticsCommon
    class GoodsTransactionListCategoriesController < ApplicationController
      protect_from_forgery with: :null_session
      before_action :set_goods_transaction_list_category, only: [:update]

      # GET /goods_transaction_list_categories
      # GET /goods_transaction_list_categories.json
      def index
        goods_list_category = params[:goods_list_type]
        @goods_transaction_list_categories = GoodsTransactionListCategory.fetch_goods_list goods_list_category
        response = MethodResponse.new(true, nil, @goods_transaction_list_categories, nil, nil)
        render json: response
      end

      # POST /goods_transaction_list_categories
      # POST /goods_transaction_list_categories.json
      def create
        @goods_transaction_list_categories = GoodsTransactionListCategory.generate_goods_list
        if @goods_transaction_list_categories
          response = MethodResponse.new(true, 'Goods list with Transaction type associated successfully!', @goods_transaction_list_categories, nil, nil)
        else
          errors = ['Error while creating Goods list Transaction type association data!']
          response = MethodResponse.new(false, nil, nil, errors, nil)
        end
        render json: response
      end

      # PATCH/PUT /goods_transaction_list_categories/1
      # PATCH/PUT /goods_transaction_list_categories/1.json
      def update
        @goods_transaction_list_category = GoodsTransactionListCategory.find(params[:id])

        if @goods_transaction_list_category.update(goods_transaction_list_category_params)
          response = MethodResponse.new(true, "Goods List updated successfully!", @goods_transaction_list_category, nil, nil)
        else
          errors = Util.error_messages @goods_transaction_list_categories, "Goods List"
          response = MethodResponse.new(false, nil, nil, errors, nil)
        end
        render json: response
      end

      private

        def set_goods_transaction_list_category
          @goods_transaction_list_category = GoodsTransactionListCategory.find(params[:id])
        end

        def goods_transaction_list_category_params
          params.require(:goods_transaction_list_category).permit(:goods_category_id, :mks_transaction_type_id, :goods_list_category_id)
        end
    end
  end
end
