module Mks
  module LogisticsCommon
    require 'ancestry'
    class GoodsCategory < ActiveRecord::Base
      has_ancestry

      validates :code, :name, presence: true, allow_blank: false
      validates :code, :name, uniqueness: true
      validates :ancestry, presence: true, if: :goods_category_type_section?

      belongs_to :goods_category_type

      def goods_category_type_section?
        goods_category_type = GoodsCategoryType.find goods_category_type_id
        if goods_category_type.code != 'SEC'
          return true
        end
      end

      def self.create_goods_category_detail (parent, goods)
        if parent == nil
          parent_id = nil
          parent_name = nil
        else
          parent_id = parent['id']
          parent_name = parent['name']
        end
        goods_category = {
            'id' => goods.id,
            'goods_category_type_id' => goods.goods_category_type_id,
            'goods_category_type_name' => goods.goods_category_type.name,
            'code' => goods.code,
            'name' => goods.name,
            'description' => goods.description,
            'hs_code' => goods.hs_code,
            'tariff_code' => goods.tariff_code,
            'unit_of_measure' => goods.unit_of_measure,
            'duty_rate' => goods.duty_rate,
            'special_permission' => goods.special_permission,
            'excise_tax' => goods.excise_tax,
            'vat' => goods.vat,
            'sur_tax' => goods.sur_tax,
            'withholding_tax' => goods.withholding_tax,
            'ss_1' => goods.ss_1,
            'ss_2' => goods.ss_2,
            'ancestry' => goods.ancestry,
            'parent_id' => parent_id,
            'parent_name' => parent_name,
            'expanded' => false,
            'data' => []
        }
        if goods.children.count == 0
          goods_category['leaf'] = true
        else
          goods_category['leaf'] = false
        end
        if parent == nil
          return goods_category
        else
          parent['data'].push(goods_category)
          return goods_category
        end
      end
    end
  end
end
