module Mks
  module LogisticsCommon
    class GoodsCategoryType < ActiveRecord::Base
      validates :code, :name, presence: true, allow_blank: false
      validates :code, :name, uniqueness: true
    end
  end
end
