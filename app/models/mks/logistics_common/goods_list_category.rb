module Mks
  module LogisticsCommon
    class GoodsListCategory < ActiveRecord::Base
      validates :code, :name, presence: true, uniqueness: true, allow_blank: false
    end
  end
end
