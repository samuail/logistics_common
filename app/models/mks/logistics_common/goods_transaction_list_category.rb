module Mks
  module LogisticsCommon
    class GoodsTransactionListCategory < ActiveRecord::Base
      validates :goods_category_id, :mks_transaction_type_id, presence: true, allow_blank: false
      validates :goods_category_id, :uniqueness => {:scope => [ :mks_transaction_type_id ]}

      belongs_to :goods_category
      belongs_to :mks_transaction_type, class_name: 'TransactionType'
      belongs_to :goods_list_category

      def goods_list_category_name
        if self.goods_list_category_id
          goods_list_category = GoodsListCategory.find self.goods_list_category_id
          goods_list_category.name
        else
          nil
        end
      end

      def self.fetch_goods_list goods_list_category
        result = []
        if goods_list_category == "0"
          goods_lists = GoodsTransactionListCategory.all
        else
          goods_lists = GoodsTransactionListCategory.where goods_list_category_id: goods_list_category.to_i
        end
        if goods_lists.count > 0
          goods_lists.each do |goods_list|
            result.push({ id: goods_list.id,
                          goods_id: goods_list.goods_category_id,
                          goods_name: goods_list.goods_category.name,
                          transaction_type_id: goods_list.mks_transaction_type_id,
                          transaction_type_name: goods_list.mks_transaction_type.name,
                          goods_list_category_id: goods_list.goods_list_category_id,
                          goods_list_category_name: goods_list.goods_list_category_name
                        })
          end
        end
        return result
      end

      def self.check_for_existing_record transaction_type_id, goods_category_id
        goods_list = GoodsTransactionListCategory.where mks_transaction_type_id: transaction_type_id,
                                                        goods_category_id: goods_category_id
        if goods_list.count == 0
          return false
        else
          return true
        end
      end

      def self.create_goods_list transaction_type_id, goods_category_id
        goods_list = GoodsTransactionListCategory.create mks_transaction_type_id: transaction_type_id,
                                                         goods_category_id: goods_category_id
        if goods_list
          return goods_list
        end
      end

      def self.generate_goods_list
        transaction_types = Mks::TransactionType.all
        goods_category_type = GoodsCategoryType.find_by_code "GOO"
        goods_categories = GoodsCategory.where goods_category_type_id: goods_category_type.id
        goods_lists = []
        transaction_types.each do |transaction_type|
          goods_categories.each do |goods_category|
            duplicate_check = check_for_existing_record transaction_type.id, goods_category.id
            if !duplicate_check
              goods_list = create_goods_list transaction_type.id, goods_category.id
              goods_lists.push ({ id: goods_list.id,
                                  goods_category_id: goods_list.goods_category_id,
                                  mks_transaction_type_id: goods_list.mks_transaction_type_id,
                                  goods_list_category_id: goods_list.goods_list_category_id })
            end
          end
        end
        if goods_lists
          return goods_lists
        end
      end
    end
  end
end
