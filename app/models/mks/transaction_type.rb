module Mks
  class TransactionType < ActiveRecord::Base
    self.table_name = 'mks_transaction_types'
    validates :code, :name, presence: true, uniqueness: true
  end
end
