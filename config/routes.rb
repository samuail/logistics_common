Mks::LogisticsCommon::Engine.routes.draw do
  resources :goods_category_types
  resources :goods_categories
  resources :goods_list_categories
  resources :goods_transaction_list_categories
end
