class CreateMksLogisticsCommonGoodsCategoryTypes < ActiveRecord::Migration
  def change
    create_table :mks_logistics_common_goods_category_types do |t|
      t.string :code
      t.string :name
      t.string :description

      t.timestamps null: false
    end
  end
end
