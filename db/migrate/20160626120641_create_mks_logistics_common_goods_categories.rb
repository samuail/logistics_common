class CreateMksLogisticsCommonGoodsCategories < ActiveRecord::Migration
  def change
    create_table :mks_logistics_common_goods_categories do |t|
      t.references :goods_category_type, index: {:name => 'goods_category_type_idx'}
      t.string :ancestry, index: {:name => 'goods_categories_index'}, foreign_key: true
      t.string :code
      t.string :description
      t.integer :duty_rate
      t.integer :excise_tax
      t.string :hs_code
      t.string :name
      t.string :special_permission
      t.integer :ss_1
      t.integer :ss_2
      t.integer :sur_tax
      t.string :tariff_code
      t.string :unit_of_measure
      t.integer :vat
      t.integer :withholding_tax

      t.timestamps null: false
    end

    add_foreign_key :mks_logistics_common_goods_categories,
                    :mks_logistics_common_goods_category_types,
                    :column => :goods_category_type_id
  end
end
