class CreateMksLogisticsCommonGoodsListCategories < ActiveRecord::Migration
  def change
    create_table :mks_logistics_common_goods_list_categories do |t|
      t.string :code
      t.string :name

      t.timestamps null: false
    end
  end
end
