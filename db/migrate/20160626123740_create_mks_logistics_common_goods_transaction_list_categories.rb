class CreateMksLogisticsCommonGoodsTransactionListCategories < ActiveRecord::Migration
  def change
    create_table :mks_logistics_common_goods_transaction_list_categories do |t|
      t.references :goods_category, index: { :name => 'goods_category_idx' }
      t.references :goods_list_category, index: { :name => 'list_category_idx' }
      t.references :mks_transaction_type, index: { :name => 'transaction_type_idx' }

      t.timestamps null: false
    end

    add_foreign_key :mks_logistics_common_goods_transaction_list_categories,
                    :mks_logistics_common_goods_categories,
                    :column => :goods_category_id
    add_foreign_key :mks_logistics_common_goods_transaction_list_categories,
                    :mks_logistics_common_goods_list_categories,
                    :column => :goods_list_category_id
    add_foreign_key :mks_logistics_common_goods_transaction_list_categories,
                    :mks_transaction_types,
                    :column => :mks_transaction_type_id
  end
end
