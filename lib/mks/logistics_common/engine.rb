module Mks
  module LogisticsCommon
    class Engine < ::Rails::Engine
      isolate_namespace Mks::LogisticsCommon

      config.generators do |g|
        g.test_framework :rspec, :fixture => false
        g.assets false
        g.helper false
      end
    end
  end
end
