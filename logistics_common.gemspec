$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "mks/logistics_common/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "mks_logistics_common"
  s.version     = Mks::LogisticsCommon::VERSION
  s.authors     = ["henock"]
  s.email       = ["henockl@live.com"]
  s.homepage    = "https://www.mks.com"
  s.summary     = "Common artifacts for the logistics app"
  s.description = "A module which contains common artifacts for the logistics app."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 4.2.6"
  s.add_dependency "ancestry"
  s.add_dependency "mks"
  s.add_dependency "mkscommon"
  s.add_development_dependency "pg"
  s.add_development_dependency "rspec-rails"

  s.test_files = Dir["spec/**/*"]
end
