require 'rails_helper'

module Mks
  module LogisticsCommon
    RSpec.describe LogisticsCommon::GoodsCategoriesController, type: :controller do
      routes { Mks::LogisticsCommon::Engine.routes}

      let(:valid_goods_category_type_attributes){
        {
            id: 1,
            code: 'SEC',
            name: 'Section',
            description: 'This is a section goods category type'
        }
      }
      let(:valid_attributes) {
        {
            goods_category_type_id: 1,
            code: 'LAAP',
            name: 'Live animals, animal products',
            description: 'A living animal and other animal products'
        }
      }

      let(:invalid_attributes) {
        {
            goods_category_type_id: 1,
            code: '',
            name: '',
            description: 'A living animal and other animal products'
        }
      }

      let(:valid_session) { {} }

      describe "GET #index" do
        it "assigns all goods_categories as @goods_categories" do
          GoodsCategoryType.create! valid_goods_category_type_attributes
          goods_category = GoodsCategory.create! valid_attributes

          get :index, format: :json

          result = JSON(response.body)

          expect(result['data']['data'][0]['code']).to eq goods_category.code
          expect(result['data']['data'][0]['name']).to eq goods_category.name
          expect(result['data']['data'][0]['description']).to eq goods_category.description
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new GoodsCategory" do
            GoodsCategoryType.create! valid_goods_category_type_attributes
            expect {
              post :create, :goods_category => valid_attributes, format: :json
            }.to change(GoodsCategory, :count).by(1)
          end

          it "assigns a newly created goods_category as @goods_category" do
            GoodsCategoryType.create! valid_goods_category_type_attributes
            post :create, :goods_category => valid_attributes, format: :json
            expect(assigns(:goods_category)).to be_a(GoodsCategory)
            expect(assigns(:goods_category)).to be_persisted
          end

          it "should return a success message" do
            GoodsCategoryType.create! valid_goods_category_type_attributes
            post :create, :goods_category => valid_attributes, format: :json
            result = JSON(response.body)
            expect(result['message']).to eq 'Section added successfully!'
          end

          it "should send duplicate error message upon goods catgeory registration" do
            GoodsCategoryType.create! valid_goods_category_type_attributes
            GoodsCategory.create! valid_attributes

            post :create, :goods_category => valid_attributes, format: :json
            result = JSON(response.body)
            expect(result['errors']).to eq ["Section Goods Code has already been taken", "Section Goods Name has already been taken"]
          end

        end

        context "with invalid params" do
          it "assigns a newly created but unsaved goods_category as @goods_category" do
            GoodsCategoryType.create! valid_goods_category_type_attributes
            post :create, :goods_category => invalid_attributes, format: :json
            expect(assigns(:goods_category)).to be_a_new(GoodsCategory)
          end

          it "should return an error message" do
            GoodsCategoryType.create! valid_goods_category_type_attributes
            post :create, :goods_category => invalid_attributes, format: :json
            result = JSON(response.body)
            expect(result['errors']).to eq ["Section Goods Code can't be blank", "Section Goods Name can't be blank"]
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) {
            {
                code: 'MEMO'
            }
          }

          it "updates the requested goods_category" do
            GoodsCategoryType.create! valid_goods_category_type_attributes
            goods_category = GoodsCategory.create! valid_attributes

            put :update, :id => goods_category.to_param, :goods_category => {code: new_attributes[:code],
                                                                             name: goods_category.name,
                                                                             description: goods_category.description,
                                                                             goods_category_type_id: goods_category.goods_category_type_id},
                format: :json
            goods_category.reload
            expect(goods_category.code).not_to eq valid_attributes[:code]
          end

          it "assigns the requested goods_category as @goods_category" do
            GoodsCategoryType.create! valid_goods_category_type_attributes
            goods_category = GoodsCategory.create! valid_attributes
            put :update, :id => goods_category.to_param, :goods_category => {code: new_attributes[:code],
                                                                             name: goods_category.name,
                                                                             description: goods_category.description,
                                                                             goods_category_type_id: goods_category.goods_category_type_id},
                format: :json
            expect(assigns(:goods_category)).to eq(goods_category)
          end

          it "should send a success message" do
            GoodsCategoryType.create! valid_goods_category_type_attributes
            goods_category = GoodsCategory.create! valid_attributes
            put :update, :id => goods_category.to_param, :goods_category => {code: new_attributes[:code],
                                                                             name: goods_category.name,
                                                                             description: goods_category.description,
                                                                             goods_category_type_id: goods_category.goods_category_type_id},
                          format: :json
            result = JSON(response.body)
            expect(result['message']).to eq "Section updated successfully!"
          end

          it "should send duplicate error message upon goods catgeory updation" do
            GoodsCategoryType.create! valid_goods_category_type_attributes
            GoodsCategory.create(code: 'MEMO',
                                 name: 'Meat and edible meat offal',
                                 description: 'Meat and other meat offal related products',
                                 goods_category_type_id: 1)
            goods_category = GoodsCategory.create! valid_attributes

            put :update, :id => goods_category.to_param, :goods_category => {code: new_attributes[:code],
                                                                             name: goods_category.name,
                                                                             description: goods_category.description,
                                                                             goods_category_type_id: goods_category.goods_category_type_id},
                format: :json
            result = JSON(response.body)
            expect(result['errors']).to eq ["Section Goods Code has already been taken"]
          end
        end

        context "with invalid params" do
          it "assigns the goods_category as @goods_category" do
            GoodsCategoryType.create! valid_goods_category_type_attributes
            goods_category = GoodsCategory.create! valid_attributes
            put :update, :id => goods_category.to_param, :goods_category => invalid_attributes, format: :json
            expect(assigns(:goods_category)).to eq(goods_category)
          end

          it "returns error message" do
            GoodsCategoryType.create! valid_goods_category_type_attributes
            goods_category = GoodsCategory.create! valid_attributes
            put :update, :id => goods_category.to_param, :goods_category => invalid_attributes, format: :json
            result = JSON(response.body)
            expect(result['errors']).to eq ["Section Goods Code can't be blank", "Section Goods Name can't be blank"]
          end
        end
      end
    end
  end
end
