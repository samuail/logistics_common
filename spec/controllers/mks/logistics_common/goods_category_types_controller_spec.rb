require 'rails_helper'

module Mks
  module LogisticsCommon
    RSpec.describe LogisticsCommon::GoodsCategoryTypesController, type: :controller do
      routes { Mks::LogisticsCommon::Engine.routes}
      let(:valid_attributes) {
        {
            code: 'SEC',
            name: 'Section',
            description: 'This is a section goods category type'
        }
      }

      describe "GET #index" do
        it "assigns all goods_category_types as @goods_category_types" do
          goods_category_type = GoodsCategoryType.create! valid_attributes
          get :index, format: :json
          expect(JSON(response.body)['data'].to_json).to eq([goods_category_type].to_json)
        end
      end
    end
  end
end
