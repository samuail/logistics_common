require 'rails_helper'

module Mks
  module LogisticsCommon
    RSpec.describe GoodsListCategoriesController, type: :controller do
      routes { Mks::LogisticsCommon::Engine.routes}

      let(:valid_attributes) {
        {
            code: 'RES',
            name: 'Restricted'
        }
      }

      describe "GET #index" do
        it "assigns all goods_list_categories as @goods_list_categories" do
          goods_list_category = GoodsListCategory.create! valid_attributes
          get :index, format: :json
          expect(JSON(response.body)['data'].to_json).to eq([goods_list_category].to_json)
        end
      end
    end
  end
end
