require 'rails_helper'

module Mks
  module LogisticsCommon
    RSpec.describe GoodsTransactionListCategoriesController, type: :controller do
      routes { Mks::LogisticsCommon::Engine.routes}
      let(:valid_section_GCT_attributes){ {id: 1, code: 'SEC', name: 'Section'} }

      let(:valid_chapter_GCT_attributes){ {id: 2, code: 'CHA', name: 'Chapter'} }

      let(:valid_heading_GCT_attributes){ {id: 3, code: 'HEA', name: 'Heading'} }

      let(:valid_goods_GCT_attributes){ {id: 4, code: 'GOO', name: 'Goods'} }

      let(:valid_section_GC_attributes){
        { id: 1,  code: 'LAAP', name: 'Live animals, animal products', description: 'A living animal and other animal products', goods_category_type_id: 1 }
      }

      let(:valid_chapter_GC_attributes){
        {id: 2, code: 'MEMO', name: 'Meat and edible meat offal', goods_category_type_id: 2, description: 'Meat and other meat offal related products', ancestry: "1"}
      }

      let(:valid_heading_GC_attributes){
        {id: 3, code: 'MBFC', name: 'Meat of bovine animals, fresh or chilled', goods_category_type_id: 3, description: 'Meet product', ancestry: "1/2"}
      }

      let(:valid_goods_GC_attributes){
        {id: 4, code: 'CHCA', name: 'Carcasses and half-carcasses', goods_category_type_id: 4, description: 'Body of a dead animal', ancestry: "1/2/3"}
      }

      let(:valid_transaction_type_attributes){
        {id: 1, code: 'IM', name: 'Import'}
      }

      let(:valid_goods_list_category_attributes){
        {id: 1, code: 'RES', name: 'Restricted'}
      }

      let(:valid_attributes) {
        { goods_category_id: 4, mks_transaction_type_id: 1 }
      }

      describe "GET #index" do
        it "assigns all goods_transaction_list_categories as @goods_transaction_list_categories" do
          GoodsCategoryType.create! valid_section_GCT_attributes
          GoodsCategoryType.create! valid_chapter_GCT_attributes
          GoodsCategoryType.create! valid_heading_GCT_attributes
          GoodsCategoryType.create! valid_goods_GCT_attributes

          GoodsCategory.create! valid_section_GC_attributes
          GoodsCategory.create! valid_chapter_GC_attributes
          GoodsCategory.create! valid_heading_GC_attributes
          GoodsCategory.create! valid_goods_GC_attributes

          Mks::TransactionType.create! valid_transaction_type_attributes

          goods_transaction_list_category = GoodsTransactionListCategory.create! valid_attributes
          get :index, :goods_list_type => "0", format: :json

          result = JSON(response.body)

          expect(result['data'][0]['goods_id']).to eq goods_transaction_list_category.goods_category_id
          expect(result['data'][0]['transaction_type_id']).to eq goods_transaction_list_category.mks_transaction_type_id
        end
      end

      describe "POST #create" do
        context "with valid params" do
          it "creates a new GoodsTransactionListCategory" do
            GoodsCategoryType.create! valid_section_GCT_attributes
            GoodsCategoryType.create! valid_chapter_GCT_attributes
            GoodsCategoryType.create! valid_heading_GCT_attributes
            GoodsCategoryType.create! valid_goods_GCT_attributes

            GoodsCategory.create! valid_section_GC_attributes
            GoodsCategory.create! valid_chapter_GC_attributes
            GoodsCategory.create! valid_heading_GC_attributes
            GoodsCategory.create! valid_goods_GC_attributes

            Mks::TransactionType.create! valid_transaction_type_attributes

            expect {
              post :create, :goods_transaction_list_category => valid_attributes, format: :json
            }.to change(GoodsTransactionListCategory, :count).by(1)
          end

          it "returns a success message" do
            GoodsCategoryType.create! valid_section_GCT_attributes
            GoodsCategoryType.create! valid_chapter_GCT_attributes
            GoodsCategoryType.create! valid_heading_GCT_attributes
            GoodsCategoryType.create! valid_goods_GCT_attributes

            GoodsCategory.create! valid_section_GC_attributes
            GoodsCategory.create! valid_chapter_GC_attributes
            GoodsCategory.create! valid_heading_GC_attributes
            GoodsCategory.create! valid_goods_GC_attributes

            Mks::TransactionType.create! valid_transaction_type_attributes

            post :create, format: :json
            result = JSON(response.body)
            expect(result["message"]).to eq "Goods list with Transaction type associated successfully!"
          end
        end
      end

      describe "PUT #update" do
        context "with valid params" do
          let(:new_attributes) {
            {goods_list_category_id: 1}
          }

          it "updates the requested goods_transaction_list_category" do
            GoodsCategoryType.create! valid_section_GCT_attributes
            GoodsCategoryType.create! valid_chapter_GCT_attributes
            GoodsCategoryType.create! valid_heading_GCT_attributes
            GoodsCategoryType.create! valid_goods_GCT_attributes

            GoodsCategory.create! valid_section_GC_attributes
            GoodsCategory.create! valid_chapter_GC_attributes
            GoodsCategory.create! valid_heading_GC_attributes
            GoodsCategory.create! valid_goods_GC_attributes

            Mks::TransactionType.create! valid_transaction_type_attributes

            goods_list_category = GoodsListCategory.create! valid_goods_list_category_attributes

            goods_transaction_list_category = GoodsTransactionListCategory.create! valid_attributes
            put :update, :id => goods_transaction_list_category.to_param, :goods_transaction_list_category => new_attributes, format: :json
            goods_transaction_list_category.reload
            expect(goods_transaction_list_category.goods_list_category_id).to eq goods_list_category.id
          end

          it "assigns the requested goods_transaction_list_category as @goods_transaction_list_category" do
            GoodsCategoryType.create! valid_section_GCT_attributes
            GoodsCategoryType.create! valid_chapter_GCT_attributes
            GoodsCategoryType.create! valid_heading_GCT_attributes
            GoodsCategoryType.create! valid_goods_GCT_attributes

            GoodsCategory.create! valid_section_GC_attributes
            GoodsCategory.create! valid_chapter_GC_attributes
            GoodsCategory.create! valid_heading_GC_attributes
            GoodsCategory.create! valid_goods_GC_attributes

            Mks::TransactionType.create! valid_transaction_type_attributes

            goods_list_category = GoodsListCategory.create! valid_goods_list_category_attributes

            goods_transaction_list_category = GoodsTransactionListCategory.create! valid_attributes
            put :update, :id => goods_transaction_list_category.to_param, :goods_transaction_list_category => valid_attributes, format: :json
            expect(assigns(:goods_transaction_list_category)).to eq(goods_transaction_list_category)
          end

          it "should return success true and success message" do
            GoodsCategoryType.create! valid_section_GCT_attributes
            GoodsCategoryType.create! valid_chapter_GCT_attributes
            GoodsCategoryType.create! valid_heading_GCT_attributes
            GoodsCategoryType.create! valid_goods_GCT_attributes

            GoodsCategory.create! valid_section_GC_attributes
            GoodsCategory.create! valid_chapter_GC_attributes
            GoodsCategory.create! valid_heading_GC_attributes
            GoodsCategory.create! valid_goods_GC_attributes

            Mks::TransactionType.create! valid_transaction_type_attributes

            GoodsListCategory.create! valid_goods_list_category_attributes

            goods_transaction_list_category = GoodsTransactionListCategory.create! valid_attributes
            put :update, :id => goods_transaction_list_category.to_param, :goods_transaction_list_category => valid_attributes, format: :json
            result = JSON(response.body)
            expect(result["success"]).to eq true
            expect(result["message"]).to eq "Goods List updated successfully!"
          end
        end
      end
    end
  end
end
