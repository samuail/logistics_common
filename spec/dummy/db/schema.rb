# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160626123740) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "mks_logistics_common_goods_categories", force: :cascade do |t|
    t.integer  "goods_category_type_id"
    t.string   "ancestry"
    t.string   "code"
    t.string   "description"
    t.integer  "duty_rate"
    t.integer  "excise_tax"
    t.string   "hs_code"
    t.string   "name"
    t.string   "special_permission"
    t.integer  "ss_1"
    t.integer  "ss_2"
    t.integer  "sur_tax"
    t.string   "tariff_code"
    t.string   "unit_of_measure"
    t.integer  "vat"
    t.integer  "withholding_tax"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "mks_logistics_common_goods_categories", ["ancestry"], name: "goods_categories_index", using: :btree
  add_index "mks_logistics_common_goods_categories", ["goods_category_type_id"], name: "goods_category_type_idx", using: :btree

  create_table "mks_logistics_common_goods_category_types", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "mks_logistics_common_goods_list_categories", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mks_logistics_common_goods_transaction_list_categories", force: :cascade do |t|
    t.integer  "goods_category_id"
    t.integer  "goods_list_category_id"
    t.integer  "mks_transaction_type_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "mks_logistics_common_goods_transaction_list_categories", ["goods_category_id"], name: "goods_category_idx", using: :btree
  add_index "mks_logistics_common_goods_transaction_list_categories", ["goods_list_category_id"], name: "list_category_idx", using: :btree
  add_index "mks_logistics_common_goods_transaction_list_categories", ["mks_transaction_type_id"], name: "transaction_type_idx", using: :btree

  create_table "mks_transaction_types", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "mks_logistics_common_goods_categories", "mks_logistics_common_goods_category_types", column: "goods_category_type_id"
  add_foreign_key "mks_logistics_common_goods_transaction_list_categories", "mks_logistics_common_goods_categories", column: "goods_category_id"
  add_foreign_key "mks_logistics_common_goods_transaction_list_categories", "mks_logistics_common_goods_list_categories", column: "goods_list_category_id"
  add_foreign_key "mks_logistics_common_goods_transaction_list_categories", "mks_transaction_types"
end
