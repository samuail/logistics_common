require 'rails_helper'

module Mks::LogisticsCommon
  RSpec.describe Mks::LogisticsCommon::GoodsCategory, type: :model do
    it "is valid with a code, name, goods category type, description, and no ancestry" do
      GoodsCategoryType.create(id: 1,
                               code: 'SEC',
                               name: 'Section',
                               description: 'This is a section goods category type')
      goods_category = GoodsCategory.new(code: 'LAAP',
                                         name: 'Live animals, animal products',
                                         goods_category_type_id: 1,
                                         description: 'A living animal and other animal products')
      expect(goods_category).to be_valid
    end

    it "is valid with a code, name, goods category type, description, and ancestry" do
      GoodsCategoryType.create(id: 1,
                               code: 'SEC',
                               name: 'Section',
                               description: 'This is a section goods category type')
      GoodsCategoryType.create(id: 2,
                               code: 'CHA',
                               name: 'Chapter',
                               description: 'This is a chapter goods category type')


      GoodsCategory.create(id: 1,
                           code: 'LAAP',
                           name: 'Live animals, animal products',
                           description: 'A living animal and other animal products',
                           goods_category_type_id: 1)
      goods_category = GoodsCategory.new(code: 'MEMO',
                                         name: 'Meat and edible meat offal',
                                         goods_category_type_id: 2,
                                         description: 'Meat and other meat offal related products',
                                         ancestry: 1)
      expect(goods_category).to be_valid
    end

    it "is invalid without a code" do
      GoodsCategoryType.create(id: 1,
                               code: 'SEC',
                               name: 'Section',
                               description: 'This is a section goods category type')
      goods_category = GoodsCategory.new(code: nil, goods_category_type_id: 1)
      goods_category.valid?
      expect(goods_category.errors[:code]).to eq ["can't be blank"]
    end

    it "is invalid without a name" do
      GoodsCategoryType.create(id: 1,
                               code: 'SEC',
                               name: 'Section',
                               description: 'This is a section goods category type')
      goods_category = GoodsCategory.new(name: nil, goods_category_type_id: 1)
      goods_category.valid?
      expect(goods_category.errors[:name]).to eq ["can't be blank"]
    end

    it "is invalid without a ancestry" do
      GoodsCategoryType.create(id: 1,
                               code: 'SEC',
                               name: 'Section',
                               description: 'This is a section goods category type')
      GoodsCategoryType.create(id: 2,
                               code: 'CHA',
                               name: 'Chapter',
                               description: 'This is a chapter goods category type')


      GoodsCategory.create(id: 1,
                           code: 'LAAP',
                           name: 'Live animals, animal products',
                           description: 'A living animal and other animal products',
                           goods_category_type_id: 1)
      goods_category = GoodsCategory.new(code: 'MEMO',
                                         name: 'Meat and edible meat offal',
                                         goods_category_type_id: 2,
                                         description: 'Meat and other meat offal related products')
      goods_category.valid?
      expect(goods_category.errors[:ancestry]).to eq ["can't be blank"]
    end

    it "is invalid with a duplicate code" do
      GoodsCategoryType.create(id: 1,
                               code: 'SEC',
                               name: 'Section',
                               description: 'This is a section goods category type')
      GoodsCategory.create! code: 'LAAP',
                            name: 'Live animals, animal products',
                            goods_category_type_id: 1
      goods_category = GoodsCategory.new(
          code: 'LAAP', goods_category_type_id: 1
      )

      goods_category.valid?
      expect(goods_category.errors[:code]).to eq ["has already been taken"]
    end

    it "is invalid with a duplicate name" do
      GoodsCategoryType.create(id: 1,
                               code: 'SEC',
                               name: 'Section',
                               description: 'This is a section goods category type')
      GoodsCategory.create! code: 'LAAP',
                            name: 'Live animals, animal products',
                            goods_category_type_id: 1
      goods_category = GoodsCategory.new(
          name: 'Live animals, animal products', goods_category_type_id: 1
      )
      goods_category.valid?
      expect(goods_category.errors[:name]).to eq ["has already been taken"]
    end

    it "returns a goods category tree" do
      GoodsCategoryType.create(id: 1,
                               code: 'SEC',
                               name: 'Section',
                               description: 'This is a section goods category type')
      GoodsCategoryType.create(id: 2,
                               code: 'CHA',
                               name: 'Chapter',
                               description: 'This is a chapter goods category type')

      GoodsCategory.create(id:1,
                           code: 'LAAP',
                           name: 'Live animals, animal products',
                           description: 'A living animal and other animal products',
                           goods_category_type_id: 1)
      GoodsCategory.create(code: 'MEMO',
                           name: 'Meat and edible meat offal',
                           description: 'Meat and other meat offal related products',
                           goods_category_type_id: 2,
                           ancestry: 1)

      goods_categories = GoodsCategory.all
      goods_category_tree = { "id" => goods_categories[0].id,
                              "code" => goods_categories[0].code,
                              "name" => goods_categories[0].name,
                              "description" => goods_categories[0].description,
                              "goods_category_type_id" => goods_categories[0].goods_category_type_id,
                              "goods_category_type_name" => goods_categories[0].goods_category_type.name,
                              "hs_code"=>nil,
                              "tariff_code"=>nil,
                              "unit_of_measure"=>nil,
                              "duty_rate"=>nil,
                              "special_permission" => nil,
                              "excise_tax" => nil,
                              "vat" => nil,
                              "sur_tax" => nil,
                              "withholding_tax" => nil,
                              "ss_1" => nil,
                              "ss_2" => nil,
                              "ancestry"=>nil,
                              "parent_name"=>nil,
                              "parent_id"=>nil,
                              "expanded" => false,
                              "data" => [],
                              "leaf" => false }
      goods_category = GoodsCategory.create_goods_category_detail nil, goods_categories[0]
      expect(goods_category).to eq goods_category_tree
    end
  end
end
