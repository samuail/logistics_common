require 'rails_helper'

module Mks
  module LogisticsCommon
    RSpec.describe GoodsCategoryType, type: :model do
      it "is valid with a code, and name" do
        goods_category_type = GoodsCategoryType.new(code: 'SEC',
                                                    name: 'Section',
                                                    description: 'This is a section goods category type')
        expect(goods_category_type).to be_valid
      end

      it "is invalid without a code" do
        goods_category_type = GoodsCategoryType.new(code: nil)
        goods_category_type.valid?
        expect(goods_category_type.errors[:code]).to include("can't be blank")
      end

      it "is invalid without a name" do
        goods_category_type = GoodsCategoryType.new(name: nil)
        goods_category_type.valid?
        expect(goods_category_type.errors[:name]).to include("can't be blank")
      end

      it "is invalid with a duplicate code" do
        GoodsCategoryType.create(code: 'SEC',
                                 name: 'Section')
        goods_category_type = GoodsCategoryType.new(code: 'SEC')
        goods_category_type.valid?
        expect(goods_category_type.errors[:code]).to include("has already been taken")
      end

      it "is invalid with a duplicate name" do
        GoodsCategoryType.create(code: 'SEC',
                                 name: 'Section')
        goods_category_type = GoodsCategoryType.new(name: 'Section')
        goods_category_type.valid?
        expect(goods_category_type.errors[:name]).to include("has already been taken")
      end
    end
  end
end
