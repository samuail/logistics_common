require 'rails_helper'

module Mks
  module LogisticsCommon
    RSpec.describe LogisticsCommon::GoodsListCategory, type: :model do
      it "is valid with a code, and name" do
        goods_list_category = GoodsListCategory.new( code: 'RES',
                                                     name: 'Restricted')
        expect(goods_list_category).to be_valid
      end

      it "is invalid without a code" do
        goods_list_category = GoodsListCategory.new(code: nil)
        goods_list_category.valid?
        expect(goods_list_category.errors[:code]).to eq ["can't be blank"]
      end

      it "is invalid without a name" do
        goods_list_category = GoodsListCategory.new(name: nil)
        goods_list_category.valid?
        expect(goods_list_category.errors[:name]).to eq ["can't be blank"]
      end

      it "is invalid with a duplicate code" do
        GoodsListCategory.create(code: 'RES',
                                 name: 'Restricted')
        goods_list_category = GoodsListCategory.new(code: 'RES')
        goods_list_category.valid?
        expect(goods_list_category.errors[:code]).to eq ["has already been taken"]
      end

      it "is invalid with a duplicate name" do
        GoodsListCategory.create(code: 'RES',
                                 name: 'Restricted')
        goods_list_category = GoodsListCategory.new(name: 'Restricted')
        goods_list_category.valid?
        expect(goods_list_category.errors[:name]).to eq ["has already been taken"]
      end
    end
  end
end
