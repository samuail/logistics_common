require 'rails_helper'

module Mks
  module LogisticsCommon
    RSpec.describe GoodsTransactionListCategory, type: :model do
      it "is valid with a goods category, and transaction type" do
        GoodsCategoryType.create(id: 1, code: 'SEC', name: 'Section')
        GoodsCategoryType.create(id: 2, code: 'CHA', name: 'Chapter')
        GoodsCategoryType.create(id: 3, code: 'HEA', name: 'Heading')
        GoodsCategoryType.create(id: 4, code: 'GOO', name: 'Goods')

        GoodsCategory.create(id: 1,  code: 'LAAP', name: 'Live animals, animal products',
                             description: 'A living animal and other animal products',
                             goods_category_type_id: 1)
        GoodsCategory.create(id: 2, code: 'MEMO', name: 'Meat and edible meat offal', goods_category_type_id: 2,
                             description: 'Meat and other meat offal related products', ancestry: "1")
        GoodsCategory.create(id: 3, code: 'MBFC', name: 'Meat of bovine animals, fresh or chilled', goods_category_type_id: 3,
                             description: 'Meet product', ancestry: "1/2")
        goods = GoodsCategory.create(id: 4, code: 'CHCA', name: 'Carcasses and half-carcasses', goods_category_type_id: 4,
                                     description: 'Body of a dead animal', ancestry: "1/2/3")

        transaction_type = Mks::TransactionType.create(id: 1, code: 'IMP', name: 'Import')

        goods_transaction_list_category = GoodsTransactionListCategory.new( goods_category_id: goods.id,
                                                                            mks_transaction_type_id: transaction_type.id)
        expect(goods_transaction_list_category).to be_valid
      end

      it "is invalid without a goods category" do
        transaction_type = Mks::TransactionType.create(id: 1, code: 'IMP', name: 'Import')

        goods_transaction_list_category = GoodsTransactionListCategory.new( goods_category_id: nil,
                                                                            mks_transaction_type_id: transaction_type.id)
        goods_transaction_list_category.valid?
        expect(goods_transaction_list_category.errors[:goods_category_id]).to eq ["can't be blank"]
      end

      it "is invalid without transaction type" do
        GoodsCategoryType.create(id: 1, code: 'SEC', name: 'Section')
        GoodsCategoryType.create(id: 2, code: 'CHA', name: 'Chapter')
        GoodsCategoryType.create(id: 3, code: 'HEA', name: 'Heading')
        GoodsCategoryType.create(id: 4, code: 'GOO', name: 'Goods')

        GoodsCategory.create(id: 1,  code: 'LAAP', name: 'Live animals, animal products',
                             description: 'A living animal and other animal products',
                             goods_category_type_id: 1)
        GoodsCategory.create(id: 2, code: 'MEMO', name: 'Meat and edible meat offal', goods_category_type_id: 2,
                             description: 'Meat and other meat offal related products', ancestry: "1")
        GoodsCategory.create(id: 3, code: 'MBFC', name: 'Meat of bovine animals, fresh or chilled', goods_category_type_id: 3,
                             description: 'Meet product', ancestry: "1/2")
        goods = GoodsCategory.create(id: 4, code: 'CHCA', name: 'Carcasses and half-carcasses', goods_category_type_id: 4,
                                     description: 'Body of a dead animal', ancestry: "1/2/3")

        goods_transaction_list_category = GoodsTransactionListCategory.new( goods_category_id: goods.id,
                                                                            mks_transaction_type_id: nil)

        goods_transaction_list_category.valid?
        expect(goods_transaction_list_category.errors[:mks_transaction_type_id]).to eq ["can't be blank"]

      end

      it "is invalid with duplicate goods category with respect to transaction type" do
        GoodsCategoryType.create(id: 1, code: 'SEC', name: 'Section')
        GoodsCategoryType.create(id: 2, code: 'CHA', name: 'Chapter')
        GoodsCategoryType.create(id: 3, code: 'HEA', name: 'Heading')
        GoodsCategoryType.create(id: 4, code: 'GOO', name: 'Goods')

        GoodsCategory.create(id: 1,  code: 'LAAP', name: 'Live animals, animal products',
                             description: 'A living animal and other animal products',
                             goods_category_type_id: 1)
        GoodsCategory.create(id: 2, code: 'MEMO', name: 'Meat and edible meat offal', goods_category_type_id: 2,
                             description: 'Meat and other meat offal related products', ancestry: "1")
        GoodsCategory.create(id: 3, code: 'MBFC', name: 'Meat of bovine animals, fresh or chilled', goods_category_type_id: 3,
                             description: 'Meet product', ancestry: "1/2")
        goods = GoodsCategory.create(id: 4, code: 'CHCA', name: 'Carcasses and half-carcasses', goods_category_type_id: 4,
                                     description: 'Body of a dead animal', ancestry: "1/2/3")

        transaction_type = Mks::TransactionType.create(id: 1, code: 'IMP', name: 'Import')

        GoodsTransactionListCategory.create( goods_category_id: goods.id,
                                             mks_transaction_type_id: transaction_type.id)

        goods_transaction_list_category = GoodsTransactionListCategory.new( goods_category_id: goods.id,
                                                                            mks_transaction_type_id: transaction_type.id)
        goods_transaction_list_category.valid?
        expect(goods_transaction_list_category.errors[:goods_category_id]).to eq ["has already been taken"]
      end

      it "return goods list category name" do
        GoodsCategoryType.create(id: 1, code: 'SEC', name: 'Section')
        GoodsCategoryType.create(id: 2, code: 'CHA', name: 'Chapter')
        GoodsCategoryType.create(id: 3, code: 'HEA', name: 'Heading')
        GoodsCategoryType.create(id: 4, code: 'GOO', name: 'Goods')

        GoodsCategory.create(id: 1,  code: 'LAAP', name: 'Live animals, animal products',
                             description: 'A living animal and other animal products',
                             goods_category_type_id: 1)
        GoodsCategory.create(id: 2, code: 'MEMO', name: 'Meat and edible meat offal', goods_category_type_id: 2,
                             description: 'Meat and other meat offal related products', ancestry: "1")
        GoodsCategory.create(id: 3, code: 'MBFC', name: 'Meat of bovine animals, fresh or chilled', goods_category_type_id: 3,
                             description: 'Meet product', ancestry: "1/2")
        goods = GoodsCategory.create(id: 4, code: 'CHCA', name: 'Carcasses and half-carcasses', goods_category_type_id: 4,
                                     description: 'Body of a dead animal', ancestry: "1/2/3")

        transaction_type = Mks::TransactionType.create(id: 1, code: 'IMP', name: 'Import')
        goods_list_category = GoodsListCategory.create(code: 'PRO', name: 'Prohibited')

        goods_list = GoodsTransactionListCategory.create( goods_category_id: goods.id,
                                                          mks_transaction_type_id: transaction_type.id,
                                                          goods_list_category_id: goods_list_category.id)

        expect(goods_list.goods_list_category_name).to eq goods_list_category.name

      end

      it "should return goods transaction list category given goods list category" do
        GoodsCategoryType.create(id: 1, code: 'SEC', name: 'Section')
        GoodsCategoryType.create(id: 2, code: 'CHA', name: 'Chapter')
        GoodsCategoryType.create(id: 3, code: 'HEA', name: 'Heading')
        GoodsCategoryType.create(id: 4, code: 'GOO', name: 'Goods')

        GoodsCategory.create(id: 1,  code: 'LAAP', name: 'Live animals, animal products',
                             description: 'A living animal and other animal products',
                             goods_category_type_id: 1)
        GoodsCategory.create(id: 2, code: 'MEMO', name: 'Meat and edible meat offal', goods_category_type_id: 2,
                             description: 'Meat and other meat offal related products', ancestry: "1")
        GoodsCategory.create(id: 3, code: 'MBFC', name: 'Meat of bovine animals, fresh or chilled', goods_category_type_id: 3,
                             description: 'Meet product', ancestry: "1/2")
        goods = GoodsCategory.create(id: 4, code: 'CHCA', name: 'Carcasses and half-carcasses', goods_category_type_id: 4,
                                     description: 'Body of a dead animal', ancestry: "1/2/3")

        transaction_type = Mks::TransactionType.create(id: 1, code: 'IMP', name: 'Import')
        goods_list_category = GoodsListCategory.create(code: 'PRO', name: 'Prohibited')

        goods_list = GoodsTransactionListCategory.create( goods_category_id: goods.id,
                                                          mks_transaction_type_id: transaction_type.id,
                                                          goods_list_category_id: goods_list_category.id)
        goods_list_fetch = GoodsTransactionListCategory.fetch_goods_list "0"

        expect(goods_list_fetch.first[:id]).to eq(goods_list.id)
        expect(goods_list_fetch.first[:goods_id]).to eq goods_list.goods_category_id
        expect(goods_list_fetch.first[:transaction_type_id]).to eq goods_list.mks_transaction_type_id
        expect(goods_list_fetch.first[:goods_list_category_id]).to eq goods_list.goods_list_category_id
      end

      it "should return true after checking for existing goods transaction record" do
        GoodsCategoryType.create(id: 1, code: 'SEC', name: 'Section')
        GoodsCategoryType.create(id: 2, code: 'CHA', name: 'Chapter')
        GoodsCategoryType.create(id: 3, code: 'HEA', name: 'Heading')
        GoodsCategoryType.create(id: 4, code: 'GOO', name: 'Goods')

        GoodsCategory.create(id: 1,  code: 'LAAP', name: 'Live animals, animal products',
                             description: 'A living animal and other animal products',
                             goods_category_type_id: 1)
        GoodsCategory.create(id: 2, code: 'MEMO', name: 'Meat and edible meat offal', goods_category_type_id: 2,
                             description: 'Meat and other meat offal related products', ancestry: "1")
        GoodsCategory.create(id: 3, code: 'MBFC', name: 'Meat of bovine animals, fresh or chilled', goods_category_type_id: 3,
                             description: 'Meet product', ancestry: "1/2")
        goods = GoodsCategory.create(id: 4, code: 'CHCA', name: 'Carcasses and half-carcasses', goods_category_type_id: 4,
                                     description: 'Body of a dead animal', ancestry: "1/2/3")

        transaction_type = Mks::TransactionType.create(id: 1, code: 'IM', name: 'Import')

        GoodsTransactionListCategory.create( goods_category_id: goods.id,
                                             mks_transaction_type_id: transaction_type.id)
        result = GoodsTransactionListCategory.check_for_existing_record transaction_type.id, goods.id
        expect(result).to eq true
      end

      it "should return false after checking for existing goods transaction record" do
        GoodsCategoryType.create(id: 1, code: 'SEC', name: 'Section')
        GoodsCategoryType.create(id: 2, code: 'CHA', name: 'Chapter')
        GoodsCategoryType.create(id: 3, code: 'HEA', name: 'Heading')
        GoodsCategoryType.create(id: 4, code: 'GOO', name: 'Goods')

        GoodsCategory.create(id: 1,  code: 'LAAP', name: 'Live animals, animal products',
                             description: 'A living animal and other animal products',
                             goods_category_type_id: 1)
        GoodsCategory.create(id: 2, code: 'MEMO', name: 'Meat and edible meat offal', goods_category_type_id: 2,
                             description: 'Meat and other meat offal related products', ancestry: "1")
        GoodsCategory.create(id: 3, code: 'MBFC', name: 'Meat of bovine animals, fresh or chilled', goods_category_type_id: 3,
                             description: 'Meet product', ancestry: "1/2")
        goods = GoodsCategory.create(id: 4, code: 'CHCA', name: 'Carcasses and half-carcasses', goods_category_type_id: 4,
                                     description: 'Body of a dead animal', ancestry: "1/2/3")

        transaction_type = Mks::TransactionType.create(id: 1, code: 'IM', name: 'Import')
        transaction_type_1 = Mks::TransactionType.create(id: 2, code: 'EX', name: 'Export')

        GoodsTransactionListCategory.create( goods_category_id: goods.id,
                                             mks_transaction_type_id: transaction_type.id)
        result = GoodsTransactionListCategory.check_for_existing_record transaction_type_1.id, goods.id
        expect(result).to eq false
      end

      it "should create goods transaction list category" do
        GoodsCategoryType.create(id: 1, code: 'SEC', name: 'Section')
        GoodsCategoryType.create(id: 2, code: 'CHA', name: 'Chapter')
        GoodsCategoryType.create(id: 3, code: 'HEA', name: 'Heading')
        GoodsCategoryType.create(id: 4, code: 'GOO', name: 'Goods')

        GoodsCategory.create(id: 1,  code: 'LAAP', name: 'Live animals, animal products',
                             description: 'A living animal and other animal products',
                             goods_category_type_id: 1)
        GoodsCategory.create(id: 2, code: 'MEMO', name: 'Meat and edible meat offal', goods_category_type_id: 2,
                             description: 'Meat and other meat offal related products', ancestry: "1")
        GoodsCategory.create(id: 3, code: 'MBFC', name: 'Meat of bovine animals, fresh or chilled', goods_category_type_id: 3,
                             description: 'Meet product', ancestry: "1/2")
        goods = GoodsCategory.create(id: 4, code: 'CHCA', name: 'Carcasses and half-carcasses', goods_category_type_id: 4,
                                     description: 'Body of a dead animal', ancestry: "1/2/3")

        transaction_type = Mks::TransactionType.create(id: 1, code: 'IM', name: 'Import')
        result = GoodsTransactionListCategory.create_goods_list transaction_type.id, goods.id
        expect(result.goods_category_id).to eq (goods.id)
        expect(result.mks_transaction_type_id).to eq (transaction_type.id)
      end

      it "should generate goods transaction type association matrix" do
        GoodsCategoryType.create(id: 1, code: 'SEC', name: 'Section')
        GoodsCategoryType.create(id: 2, code: 'CHA', name: 'Chapter')
        GoodsCategoryType.create(id: 3, code: 'HEA', name: 'Heading')
        GoodsCategoryType.create(id: 4, code: 'GOO', name: 'Goods')

        GoodsCategory.create(id: 1,  code: 'LAAP', name: 'Live animals, animal products',
                             description: 'A living animal and other animal products',
                             goods_category_type_id: 1)
        GoodsCategory.create(id: 2, code: 'MEMO', name: 'Meat and edible meat offal', goods_category_type_id: 2,
                             description: 'Meat and other meat offal related products', ancestry: "1")
        GoodsCategory.create(id: 3, code: 'MBFC', name: 'Meat of bovine animals, fresh or chilled', goods_category_type_id: 3,
                             description: 'Meet product', ancestry: "1/2")
        goods = GoodsCategory.create(id: 4, code: 'CHCA', name: 'Carcasses and half-carcasses', goods_category_type_id: 4,
                                     description: 'Body of a dead animal', ancestry: "1/2/3")

        transaction_type_1 = Mks::TransactionType.create(id: 1, code: 'IM', name: 'Import')
        transaction_type_2 = Mks::TransactionType.create(id: 2, code: 'EX', name: 'Export')

        result = GoodsTransactionListCategory.generate_goods_list

        expect(result.count).to eq 2
        expect(result.first[:goods_category_id]).to eq goods.id
        expect(result.first[:mks_transaction_type_id]).to eq transaction_type_1.id
        expect(result.last[:mks_transaction_type_id]).to eq transaction_type_2.id
      end
    end
  end
end
